'use strict';
const SolarSystem = require('./entities/solar-system');
const Predictor = require('./predictor');
const Logger = require('./util/logger');

const AMOUNT_DAYS = (366 * 3 + 365 * 7); // In 10 years almost 3 leap-year

const days = process.argv.slice(2);
const resultTenYears = Predictor.process(days[0] || AMOUNT_DAYS);

Logger.showPredictionConsole(resultTenYears);
Logger.showResume(resultTenYears);
