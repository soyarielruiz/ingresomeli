'use strict';

const planetFactory = require('./planetFactory');
const PlanetsSingleton = require('./planets-charger');

const planetListSingleton = new PlanetsSingleton().getInstance().getPlanets();

class SolarSystem {

    constructor() {
        this._solarPlanets = [new planetFactory(planetListSingleton['Vu']),
            new planetFactory(planetListSingleton['Fe']),
            new planetFactory(planetListSingleton['Be'])];
    }

    get solarPlanets() {
        return this._solarPlanets;
    }

    moveAllPlanets(givenDay) {
        this.solarPlanets.forEach( planet => {
            planet.move(givenDay);
        });
    }
}

module.exports = SolarSystem;