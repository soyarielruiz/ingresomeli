'use strict';

const INITIAL_ANGLE = 0;
const FULL_TURN = 360;

class Orbit {

    constructor(sunDistance, angularVelocity) {
        this.sunDistance = sunDistance;
        this.angularVelocity = angularVelocity;
        this.actualAngle = INITIAL_ANGLE;
    }

    get actualAngle() {
        return this._actualAngle;
    }

    set actualAngle(value) {
        this._actualAngle = value;
    }

    get angularVelocity() {
        return this._angularVelocity;
    }

    set angularVelocity(value) {
        this._angularVelocity = value;
    }

    get sunDistance() {
        return this._sunDistance;
    }

    set sunDistance(value) {
        this._sunDistance = value;
    }

    move(day) {
        this.actualAngle = (day * this.angularVelocity) % FULL_TURN;
    }

    transformToCartesian() {
        return {
            x: this.sunDistance * Math.cos(this.actualAngle),
            y: this.sunDistance * Math.sin(this.actualAngle)
        }
    }
}

module.exports = Orbit;