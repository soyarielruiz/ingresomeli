'use strict';

const Orbit = require('./orbit');

class PlanetFactory {

    constructor(planet) {
        //Suppose a valid name
        const {name, angular_velocity, sun_distance} = planet;
        this.name = name;
        this.orbit = new Orbit(sun_distance, angular_velocity);
    }

    get orbit() {
        return this._orbit;
    }

    set orbit(value) {
        this._orbit = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    move(day) {
        this.orbit.move(day);
    }
}

module.exports = PlanetFactory;