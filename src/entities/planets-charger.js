'use strict';

const fs = require('fs');
const path = require('path');

class Charger {

    constructor() {
        this.planets = {};
    }

    /**
     * Load file and return solar system planet list
     * @returns {Object} Planet list
     */
    getPlanets() {
        let rawData = fs.readFileSync(path.resolve(__dirname, './../config/system-solar-planets.json'), 'UTF-8');
        let configSystem = JSON.parse(rawData);
        return configSystem['planets'];
    }

}

class PlanetsSingleton {

    /**
     * Creates the instance if dont exists
     */
    constructor() {
        if (!PlanetsSingleton.instance) {
            PlanetsSingleton.instance = new Charger();
        }
    }

    /**
     * Gets the instance
     * @returns {Charger}
     */
    getInstance() {
        return PlanetsSingleton.instance;
    }
}

module.exports = PlanetsSingleton;