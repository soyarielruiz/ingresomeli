'use strict';
const express = require('express');
const route = express.Router();
const ForecastPredictor = require('./../api/controllers/forecast-prediction');

// Forecast Predictor get a Day
route.get('/api/clima/:dia', async (req, res) => {
    try {
        const resultPredictor = await ForecastPredictor.getPredictionDay(req.params.dia);
        res.json('Resultado: ' + resultPredictor);
    } catch (error) {
        res.status(400).json({error: true, message: 'Error when gets from database' + error.message});
    }
});

// Forecast Predictor Save
route.get('/api/pronosticos/:cantidad_dias', async (req, res) => {
    try {
        await ForecastPredictor.SavePredictions(req.params.cantidad_dias);
        res.json('Pronosticos guardados');
    } catch (error) {
        res.status(400).json({error: true, message: 'Invalid JSON'});
    }
});


// When dont go to a right uri
route.get('/', async (req, res) => {
    try {
        res.json('Solo se puede acceder a la siguiente ruta /api/clima/{dia} para obtener el pronostico de un dia ' +
            'ó /api/pronosticos/{cantidad de días} si quiere guardar dias no pronosticados');
    } catch (error) {
        res.status(400).json({error: true, message: 'Error processing'});
    }
    res.end();
});



module.exports = route;