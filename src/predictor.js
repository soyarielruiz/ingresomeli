'use strict';
const SolarSystem = require('./entities/solar-system');
const ForecastMapper = require('./weather-forecast/forecast-mapper');

const AMOUNT_DAYS = (366 * 3 + 365 * 7); // In 10 years almost 3 leap-year
const SUN_POSITION = { x: 0, y: 0 };

class Predictor {

    /**
     * To get the days constant of 10 years
     * @returns {number}
     */
    static get amountDays() {
        return AMOUNT_DAYS;
    }

    /**
     * Process the days and create forecast prediction
     * @param {Number} totalDays Given days to predict
     * @returns {Array} Prediction given day a day
     */
    static process(totalDays) {
        const daysToPredict = this.getDaysToPredict(totalDays);
        const solarSystem = new SolarSystem();

        return this.getSolarSystemForecast(solarSystem, daysToPredict);
    }

    /**
     * Get the number given or days constant
     * @param {Number} totalDays Given number of days
     * @returns {*|number} Days to predict
     */
    static getDaysToPredict(totalDays) {
        let daysToUse = totalDays;

        if (isNaN(totalDays) || totalDays <= 0) {
            // we use 10 years in days
            daysToUse = this.amountDays;
        }
        return daysToUse;
    }

    /**
     * Get forecast for total given days
     * @param {Object} solarSystem Solar system Planet list
     * @param {Number} daysToPredict Days amount to predict
     * @returns {Array} With forecast given
     */
    static getSolarSystemForecast(solarSystem, daysToPredict) {
        const daysToUse = [...new Array(parseInt(daysToPredict))];

        return daysToUse.map((value, index) => {
            // index beginning in 0, but we need that starts in 1
            const resultForecast = ForecastMapper.getForecast(index + 1, solarSystem, SUN_POSITION);
            return { day:index + 1,
                drought: resultForecast[0],
                rainy: resultForecast[1][0],
                rainy_optimal: resultForecast[1][1],
                optimal: resultForecast[2],
                created: new Date()
            };
        });
    }
}

module.exports = Predictor;
