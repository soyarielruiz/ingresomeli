'use strict';
const MathHelper = require('./math-helper');
const LinearCalculator = require('./linear-calculator');

const MAXIMUM_INTENSITY = 6312.06054133542; //Maximum rainfall intensity calculated

class TriangleCalculator {

    /**
     * Validates if isn't a linear then is a triangle
     * @param {Array} solarSystem Planet list
     * @returns {boolean} If is triangle or not
     */
    static isATriangle(solarSystem) {
        return !(LinearCalculator.areInLine(solarSystem));
    }

    /**
     * Validates if planet list are equals to maximum intensity
     * @param {Array} solarSystem Planet list
     * @returns {Boolean} If is maximum or not
     */
    static isMaximumIntensity(solarSystem) {
        const { firstPlanetPosition, secondPlanetPosition, thirdPlanetPosition } = this.getCartesianPositions(solarSystem);

        return (MathHelper.isEqual(this.getPerimeter(
            firstPlanetPosition,
            secondPlanetPosition,
            thirdPlanetPosition), MAXIMUM_INTENSITY)
        );
    }

    /**
     * Returns the solar system planets in cartesian points
     * @param {Array} planets The planet list to use in prediction
     * @returns {{firstPlanetPosition: {x, y}, secondPlanetPosition: {x, y}, thirdPlanetPosition: {x, y}}}
     */
    static getCartesianPositions(planets) {
        const firstPlanetPosition = planets.solarPlanets[0].orbit.transformToCartesian();
        const secondPlanetPosition = planets.solarPlanets[1].orbit.transformToCartesian();
        const thirdPlanetPosition = planets.solarPlanets[2].orbit.transformToCartesian();

        return { firstPlanetPosition, secondPlanetPosition, thirdPlanetPosition };
    }

    /**
     * Returns perimeter between the planets
     * @param firstPlanetPosition
     * @param secondPlanetPosition
     * @param thirdPlanetPosition
     * @returns {*}
     */
    static getPerimeter( firstPlanetPosition, secondPlanetPosition, thirdPlanetPosition) {
        return ( MathHelper.getPerimeterTriangle(firstPlanetPosition, secondPlanetPosition, thirdPlanetPosition));
    }

    /**
     *
     * @param sunPosition
     * @param planets
     * @returns {boolean|*}
     */
    static haveTheSunInside(sunPosition, planets) {
        const totalArea = MathHelper.calculateAreaTriangle( planets.solarPlanets[0].orbit.transformToCartesian(),
            planets.solarPlanets[1].orbit.transformToCartesian(), planets.solarPlanets[2].orbit.transformToCartesian());

        //validates if exists area between the 3 points
        if(totalArea <= 0) {
            return false;
        }

        const firstArea = MathHelper.calculateAreaTriangle(sunPosition,
            planets.solarPlanets[0].orbit.transformToCartesian(), planets.solarPlanets[1].orbit.transformToCartesian());

        const secondArea = MathHelper.calculateAreaTriangle(planets.solarPlanets[1].orbit.transformToCartesian(),
            sunPosition, planets.solarPlanets[2].orbit.transformToCartesian());

        const thirdArea = MathHelper.calculateAreaTriangle(planets.solarPlanets[2].orbit.transformToCartesian(),
            planets.solarPlanets[0].orbit.transformToCartesian(), sunPosition);

        return (MathHelper.isEqual(totalArea, (firstArea + secondArea + thirdArea)));
    }
}

module.exports = TriangleCalculator;