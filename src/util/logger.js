'use strict';

class Logger {

    static showPredictionConsole(predictionList) {
        const predictionListWithOutDate = this.removeDate(predictionList);
        return this.addColorMessage(predictionListWithOutDate);
    }

    static showPrediction(predictionList) {
        return this.parseToString(this.removeDate(predictionList));
    }

    static removeDate(predictionList) {
        return predictionList.map( predictionDay => {
            delete predictionDay.created;
            return predictionDay;
        });
    }

    static parseToString(predictionList) {
        return ("Day: " + predictionList[0].day
            + " is drought: " + predictionList[0].drought
            + " is rainy: " + predictionList[0].rainy
            + " is rainy optimal: " + predictionList[0].rainy_optimal
            + " is optimal: " + predictionList[0].optimal
        );
    }

    static parseResumeToString(predictionResume) {
        return console.log("\x1b[33m%s\x1b[0m", "Resume:",
            " drought quantity: ", predictionResume.drought,
            ", rainy quantity: " + predictionResume.rainy,
            ", rainy optimal quantity: ", predictionResume.rainy_optimal,
            ", optimal quantity: ", predictionResume.optimal
        );
    }

    static addColorMessage(predictionList) {
        return predictionList.map( predictionDay => {
            return [console.log("\x1b[33m%s\x1b[0m", "Day: ", predictionDay.day),
            console.log("\x1b[33m", " is drought: ", "\x1b[0m", predictionDay.drought),
            console.log("\x1b[33m", " is rainy: ", "\x1b[0m", predictionDay.rainy),
            console.log("\x1b[33m", " is rainy optimal: ", "\x1b[0m", predictionDay.rainy_optimal),
            console.log("\x1b[33m", " is optimal: ", "\x1b[0m", predictionDay.optimal)
            ]
        });
    }

    static showResume(resultTenYears) {
        const resumeWeather = {
            drought: 0,
            rainy: 0,
            rainy_optimal: 0,
            optimal: 0,
        };

        resultTenYears.forEach( predictionDay => {
            resumeWeather.drought += predictionDay.drought ? 1 : 0;
            resumeWeather.rainy += predictionDay.rainy ? 1 : 0;
            resumeWeather.rainy_optimal += predictionDay.rainy_optimal ? 1 : 0;
            resumeWeather.optimal += predictionDay.optimal ? 1 : 0;
        });

        return this.parseResumeToString(resumeWeather);
    }
}

module.exports = Logger;