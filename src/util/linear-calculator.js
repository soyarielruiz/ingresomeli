'use strict';

const MathHelper = require('./math-helper');

class LinearCalculator {

    /**
     *
     * @param sunPosition
     * @param solarSystem
     * @returns {boolean}
     */
    static areInLineWithSun(sunPosition, solarSystem) {
        const slope = LinearCalculator.createLinearFunction(
            solarSystem.solarPlanets[0].orbit.transformToCartesian(),
            sunPosition);

        return this.fulfillScope(solarSystem, slope);
    }

    /**
     *
     * @param solarSystem
     * @returns {boolean}
     */
    static areInLine(solarSystem) {
        const slope = LinearCalculator.createLinearFunction(
            solarSystem.solarPlanets[0].orbit.transformToCartesian(),
            solarSystem.solarPlanets[1].orbit.transformToCartesian());

        return this.fulfillScope(solarSystem, slope);
    }

    /**
     *
     * @param solarSystem
     * @param slope
     * @returns {boolean}
     */
    static fulfillScope(solarSystem, slope) {
        let booleanCondition = true;

        solarSystem.solarPlanets.forEach( planet => {
            const {x, y} = planet.orbit.transformToCartesian();
            booleanCondition = booleanCondition && MathHelper.isEqual((x * slope), y);
        });

        return booleanCondition;
    }

    /**
     *
     * @param firstPosition
     * @param secondPosition
     * @returns {number}
     */
    static createLinearFunction( firstPosition, secondPosition) {
        const { x: firstX, y: firstY } = firstPosition;
        const { x: secondX, y: secondY } = secondPosition;
        return ((secondY - firstY) / (secondX - firstX));
    }
}

module.exports = LinearCalculator;