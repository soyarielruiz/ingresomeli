'use strict';

const MINIMUM_DELTA = 0.0000000001;

class MathHelper {

    static calculateAreaTriangle(firstPosition, secondPosition, thirdPosition) {
        const { x: firstX, y: firstY } = firstPosition;
        const { x: secondX, y: secondY } = secondPosition;
        const { x: thirdX, y: thirdY } = thirdPosition;

        return (Math.abs((firstX - thirdX) * (secondY - firstY)
                -  (firstX - secondX) * (thirdY - firstY) ) * 0.5);
    }

    static getPerimeterTriangle(firstPosition, secondPosition, thirdPosition) {
        return (this.getDistanceBetweenPoints(firstPosition, secondPosition)
                + this.getDistanceBetweenPoints(secondPosition, thirdPosition)
                + this.getDistanceBetweenPoints(thirdPosition, firstPosition)
        );
    }

    /**
     * Get Euclidean distance
     * @param {Object} firstPoint
     * @param {Object} secondPoint
     * @returns {number} The distance between 2 points
     */
    static getDistanceBetweenPoints( firstPoint, secondPoint) {
        const { x: firstX, y: firstY } = firstPoint;
        const { x: secondX, y: secondY } = secondPoint;
        return Math.sqrt(Math.pow(secondY - firstY, 2) + Math.pow(secondX - firstX, 2));
    }

    /**
     * Validates equality of two double numbers
     * @param fistValue
     * @param secondValue
     * @returns {boolean}
     */
    static isEqual( fistValue, secondValue) {
        return (Math.abs(fistValue - secondValue) < MINIMUM_DELTA);
    }
}

module.exports = MathHelper;