'use strict';

const Prediction = require('./../../predictor');
const Forecast = require('./../models/forecast');
const Logger = require('./../../util/logger');

class ForecastPrediction {

    /**
     * Gets a day, looking in database and returns if have any forecast
     * @param givenDay
     * @returns {Promise<string>}
     */
    static async getPredictionDay(givenDay) {

        if (givenDay < 0) {
            return "El numero debe ser mayor a 0"
        }

        try {
            const resultDay = await Forecast.findPrediction(givenDay);
            return Logger.showPrediction(resultDay);
        } catch (e) {
            throw new Error(e.message);
        }
    }

    static async SavePredictions(amountDays) {
        try {
            const toDo = this.saveByBatch(amountDays);
        } catch (e) {
            throw new Error(e.message);
        }
    }

    static async saveByBatch(amountDays) {
        const resultToSave = Prediction.process(amountDays);

        let i, j, chunkToSave, chunk = 200;
        //Loop over all array and creates a chunk
        for (i=0,j=resultToSave.length; i < j; i += chunk) {
            chunkToSave = resultToSave.slice(i, i+chunk);
            try {
                await Forecast.savePrediction(chunkToSave);
            } catch (e) {
                throw new Error(e.message);
            }

        }
        // If goes here everything is Ok
        return true;

    }
}

module.exports = ForecastPrediction;