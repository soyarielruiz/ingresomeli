'use strict';
const mongoose = require('mongoose');
require('dotenv').config();

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
} = process.env;

const options = {
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    connectTimeoutMS: 10000,
    useUnifiedTopology: true
};

const url = `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}/${MONGO_DB}`;

mongoose.connect(url, options).then( function() {
    console.log('MongoDB is connected');
})
.catch( (err) =>
        console.log(err.message)
);

module.exports = mongoose;