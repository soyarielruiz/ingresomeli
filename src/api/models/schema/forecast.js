'use strict';

const mongoose = require('mongoose');

const forecastSchema = new mongoose.Schema({
    day: {
        type: Number,
        required: [true, 'Day is required']
    },
    drought: {
        type: Boolean,
        required: [true, 'Drought is required']
    },
    rainy: {
        type: Boolean,
        required: [true, 'Rainy is required']
    },
    rainy_optimal: {
        type: Boolean,
        required: [true, 'Rainy Optimal is required']
    },
    optimal: {
        type: Boolean,
        required: [true, 'Optimal weather is required']
    },
    created: {
        type: Date,
        required: [true, 'Created date is required']
    }
});

module.exports = forecastSchema;