'use strict';

const mongoose = require('./db');
const forecastSchema = require('./schema/forecast');
const Prediction = mongoose.model('meli', forecastSchema, 'forecastCollection');

class ForecastModel {

    static async savePrediction(PredictionList) {
        try {
            return Prediction.insertMany( PredictionList, { ordered: true });
        } catch (e) {
            throw new Error('Error: ' + e.message);
        }
    }

    static async findPrediction(day) {
        try {
            return await Prediction.find({day});
        } catch (e) {
            throw new Error('Error: ' + e.message)
        }

    }

}

module.exports = ForecastModel;