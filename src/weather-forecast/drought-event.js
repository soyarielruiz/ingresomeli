'use strict';

const ForecastEventInterface = require('./forecast-event-interface');
const LinearCalculator = require('./../util/linear-calculator');

class DroughtEvent extends ForecastEventInterface {

    static getPredictionForecast(sunPosition, planets) {
        return LinearCalculator.areInLineWithSun(sunPosition, planets);
    }
}

module.exports = DroughtEvent;