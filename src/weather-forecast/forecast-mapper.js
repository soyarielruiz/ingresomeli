const DroughtEvent = require('./drought-event');
const OptimalEvent = require('./optimal-event');
const RainyEvent = require('./rainy-event');

class ForecastMapper {

    /**
     * Returns Strategy Forecast list
     * @returns {*[]}
     */
    static get possibleForecast() {
        return [ DroughtEvent, RainyEvent, OptimalEvent];
    }

    /**
     * Get solar system forecast for all weather events
     * @param dayToPredict
     * @param solarSystem
     * @param sunPosition
     * @returns {(*[]|*)[]}
     */
    static getForecast(dayToPredict, solarSystem, sunPosition) {
        return this.possibleForecast.map( weather => {
            // Move a day
            solarSystem.moveAllPlanets(dayToPredict);
            return weather.getPredictionForecast(sunPosition, solarSystem);
        });
    }
}

module.exports = ForecastMapper;