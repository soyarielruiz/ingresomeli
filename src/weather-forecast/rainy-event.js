'use strict';

const ForecastEventInterface = require('./forecast-event-interface');
const TriangleCalculator = require('./../util/triangle-calculator');

class RainyEvent extends ForecastEventInterface {

    static getPredictionForecast(sunPosition, planets) {
        let isMaximumIntensity = false;

        const isATriangle = TriangleCalculator.isATriangle(planets);
        const haveSunInside = TriangleCalculator.haveTheSunInside(sunPosition, planets);

        if(isATriangle && haveSunInside) {
            isMaximumIntensity = TriangleCalculator.isMaximumIntensity(planets);
        }
        return [ haveSunInside, isMaximumIntensity];
    }
}

module.exports = RainyEvent;