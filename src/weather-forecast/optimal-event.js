'use strict';

const ForecastEventInterface = require('./forecast-event-interface');
const LinearCalculator = require('./../util/linear-calculator');

class OptimalEvent extends ForecastEventInterface {

    static getPredictionForecast(sunPosition, solarSystem) {
        return ( !LinearCalculator.areInLineWithSun(sunPosition, solarSystem)
            && LinearCalculator.areInLine(solarSystem)
        );
    }
}

module.exports = OptimalEvent;