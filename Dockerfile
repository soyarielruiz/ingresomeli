FROM node:10

WORKDIR /usr/src

COPY package.json ./

RUN npm install --quiet

COPY src .

CMD ["npm", "run", "dev"]