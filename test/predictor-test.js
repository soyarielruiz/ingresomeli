'use strict';

const expect = require("chai").expect;
const should = require('should');
const sinon = require("sinon");

const assert = require('assert');
const Predictor = require('../src/predictor');

describe("Predictor Test", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
        sinon.useFakeTimers(new Date(2019, 10, 15).getTime());
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Testing Predictor', function () {

        it('Prediction by one Day.....', () => {
            const nowDate = new Date();
            assert.deepStrictEqual(Predictor.process(1), [ { day: 1,
                drought: false,
                rainy: false,
                rainy_optimal: false,
                optimal: false,
                created: nowDate
            }] );
        });

        it('Get amount days in ten years.....', () => {
            assert.deepStrictEqual(Predictor.amountDays, 3653);
        });

        it('Get amount days in ten years if not defined any days amount.....', () => {
            assert.deepStrictEqual(Predictor.getDaysToPredict("asd"), 3653);
        });
    });
});