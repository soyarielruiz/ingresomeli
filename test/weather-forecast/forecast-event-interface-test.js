'use strict';

const expect = require("chai").expect;
const should = require('should');
const sinon = require("sinon");
const sandbox = sinon.createSandbox();

const assert = require('assert');
const ForecastEventInterface = require('./../../src/weather-forecast/forecast-event-interface');

describe("Rainy Event", function () {

    const position = {
        x: 10,
        y: 10
    };

    const sunPosition = {
        x: 0,
        y: 0
    };

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Testing', function () {

        it('Validates undefined for be a interface.....', () => {
            assert.deepStrictEqual(ForecastEventInterface.getPredictionForecast({ x:0, y:0}, []), undefined);
        });

    });
});