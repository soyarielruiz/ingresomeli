'use strict';

const expect = require("chai").expect;
const should = require('should');
const sinon = require("sinon");
const sandbox = sinon.createSandbox();

const assert = require('assert');
const RainyEvent = require('./../../src/weather-forecast/rainy-event');
const TriangleCalculator = require('./../../src/util/triangle-calculator');

describe("Rainy Event", function () {

    const position = {
        x: 10,
        y: 10
    };

    const sunPosition = {
        x: 0,
        y: 0
    };

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Testing', function () {

        it('Validates that not is in maximum intensity.....', () => {
            sandbox.stub(TriangleCalculator, 'isATriangle').returns(true);
            sandbox.stub(TriangleCalculator, 'haveTheSunInside').returns(true);
            sandbox.stub(TriangleCalculator, 'isMaximumIntensity').returns(false);
            assert.deepStrictEqual(RainyEvent.getPredictionForecast({ x:0, y:0}, []), [true, false]);
        });

    });
});