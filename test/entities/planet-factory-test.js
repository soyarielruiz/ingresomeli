'use strict';

const expect = require("chai").expect;
const should = require('should');
const assert = require('assert');
const sandbox = require('sinon').createSandbox();

const PlanetFactory = require('../../src/entities/planetFactory');
const PlanetsSingleton = require('../../src/entities/planets-charger');

const planetList = new PlanetsSingleton().getInstance().getPlanets();


describe("Orbit Test", function () {

    const planetExample = new PlanetFactory(planetList['Vu']);

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        sandbox.restore();
    });

    describe('Unit Test', function () {

        it('Validates orbit existent.....', () => {
            assert.deepStrictEqual(planetExample.orbit.actualAngle, 0);
        });

        it('Validates name.....', () => {
            assert.deepStrictEqual(planetExample.name, 'Vulcano');
        });

        it('Validates when pass one day if planet moves appropriate.....', () => {
            planetExample.move(1);
            assert.deepStrictEqual(planetExample.orbit.actualAngle,  5);
        });

        it('Validates when pass one year if planet returns to the initial angle.....', () => {
            planetExample.move(360);
            assert.deepStrictEqual(planetExample.orbit.actualAngle, 0);
        });

    });
});