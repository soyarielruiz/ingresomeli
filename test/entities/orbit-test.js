'use strict';

const expect = require("chai").expect;
const should = require('should');
const assert = require('assert');

const Orbit = require('../../src/entities/orbit');

describe("Orbit Test", function () {
    const orbit = new Orbit(10, 1);

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Unit Test', function () {

        it('Validate transformation to Cartesian.....', function () {
            assert.deepStrictEqual(orbit.transformToCartesian(), {x: 10, y: 0});
        });

        it('Validate angle in one year.....', function () {
            orbit.move(360);
            assert.deepStrictEqual(orbit.actualAngle, 0);
        });

        it('Validate angle.....', function () {
            orbit.move(361);
            assert.deepStrictEqual(orbit.actualAngle, 1);
        });
    });
});