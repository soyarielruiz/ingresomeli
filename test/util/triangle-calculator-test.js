'use strict';

const expect = require("chai").expect;
const should = require('should');
const sinon = require("sinon");
const sandbox = sinon.createSandbox();

const assert = require('assert');
const TriangleCalculator = require('./../../src/util/triangle-calculator');
const MathHelper = require('./../../src/util/math-helper');


describe("TriangleCalculator", function () {

    const position = {
        x: 10,
        y: 10
    };

    const sunPosition = {
        x: 0,
        y: 0
    };

    const planetListRight = { solarPlanets: [
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: 10, y: 10};
                    }
                }
            },
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: -5, y: 10 };
                    }
                }
            },
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: -5, y: -5 };
                    }
                }
            }
        ]
    };

    const planetList2 = { solarPlanets: [
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: 9, y: 10};
                    }
                }
            },
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: 9, y: 9 };
                    }
                }
            },
            {   orbit : {
                    transformToCartesian: function () {
                        return { x: 9, y: 8 };
                    }
                }
            }
        ]
    };

    const resultPL2 = {
        firstPlanetPosition: { x: 9, y: 10 },
        secondPlanetPosition: { x: 9, y: 9 },
        thirdPlanetPosition: { x: 9, y: 8 }
    };

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        sandbox.restore();
    });

    describe('Testing Triangle Calculator', function () {

        it('Get triangle perimeter .....', () => {
            sandbox.stub(MathHelper, 'getPerimeterTriangle').returns(10);
            assert.deepStrictEqual(TriangleCalculator.getPerimeter(position, position, position), 10);
        });

        it('Have the sun inside.....', () => {
            assert.deepStrictEqual(TriangleCalculator.haveTheSunInside(sunPosition, planetListRight), true);
        });

        it('Don\' have the sun inside.....', () => {
            assert.deepStrictEqual(TriangleCalculator.haveTheSunInside(sunPosition, planetList2), false);
        });

        it('Validates cartesian positions .....', () => {
            assert.deepStrictEqual(TriangleCalculator.getCartesianPositions(planetList2), resultPL2);
        });

        it('Validates if gets maximum intensity.....', () => {
            assert.deepStrictEqual(TriangleCalculator.isMaximumIntensity(planetList2), false);
        });


    });
});