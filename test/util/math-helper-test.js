'use strict';

const expect = require("chai").expect;
const should = require('should');
const sinon = require("sinon");

const assert = require('assert');
const MathHelper = require('./../../src/util/math-helper');

describe("MathHelper", function () {

    const firstPosition = {
        x: 10,
        y: 10
    };

    const secondPosition = {
        x: 15,
        y: 10
    };

    const thirdPosition = {
        x: 12,
        y: 10
    };

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Testing', function () {

        it('Get triangle perimeter .....', () => {
            assert.deepStrictEqual(MathHelper.getPerimeterTriangle(firstPosition, secondPosition, thirdPosition), 10);
        });

        it('Get distance between 2 points.....', () => {
            assert.deepStrictEqual(MathHelper.getDistanceBetweenPoints(firstPosition, secondPosition), 5);
        });

        it('Validate if 2 values are not equals.....', () => {
            assert.deepStrictEqual(MathHelper.isEqual(3.1111111, 3.1111114), false);
        });

        it('Validate if 2 values are equals.....', () => {
            assert.deepStrictEqual(MathHelper.isEqual(3.1111111111, 3.1111111111), true);
        });

    });
});