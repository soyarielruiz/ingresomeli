# IngresoMeli

**Test de Ingreso**

#### Este trabajo consta en el test de ingreso en el cual se debe predecir el tiempo en los próximos 10 años:

Si se desea levantar el entorno local se debe tener instalado docker y luego seguir los siguientes pasos:
>Para poder lanzar el contenedor se debe buildear previamente el contenedor con:

``` docker-compose build ```

>Para poder lanzar el contenedor se debe buildear previamente el contenedor con:

``` docker-compose up -d ```

>Si no quisiera instalar docker puede hacerlo instalando node y npm, luego lanzando:

``` npm run start ```

>Luego posee los siguientes endpoints para acceder:

+ [localhost:4005/api/clima/{diaABuscar}](http://localhost:4005/api/clima/dia) (Para obtener info de un día en particular) 
+ [localhost:4005/api/pronosticos/{cantidadDeDiasAGuardar}](http://localhost:4005/api/pronosticos/cantidadDias) (para guardar días que no existan, secuencialmente desde 1) 

En caso de querer verlo online:

[App Engine Obtener Clima](https://meliingreso.appspot.com/api/clima/dia)  
[App Engine Guardar pronosticos](https://meliingreso.appspot.com/api/pronosticos/cantidadDias) 

Para obtener las predicciones por consola se debe ingresar el siguiente comando por consola:

+ `npm run predictor` (dentro de la carpeta raíz)

***
 
[Repositorio](https://gitlab.com/soyarielruiz/ingresomeli)

  
**Hecho por Ariel Ruiz.**